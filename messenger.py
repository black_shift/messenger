import smtplib

from os.path import basename

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication

from slacker import Slacker

class Messenger():
    def __init__(self):
        #TODO change to bot email and password
        self.sender = 'xxx.xxx@xxx.com.br'
        self.passwd = 'xxxxxx'
        self.footerHTML = 'footer.htm'
        self.logoIMG = 'logo.jpg'
        self.slackAPIToken = 'slack-token'
    
    def __getFooter(self):
        footer = ''
        with open(self.footerHTML, mode='r', encoding='utf-8') as f:
            footer = f.read()
        return footer

    def __getAllRecipients(self, recipients):
        recipients_string = ''
        for recipient in recipients:
            recipients_string += recipients_string + recipient + ','
        return recipients_string
    
    def send_email(self, subject, recipients, message, attachments=None):
        ''' Envia Email \n
        :param subject: Email de envio \n
        :param recipients: Array de destinatários \n
        :param message: Mensagem a ser enviada \n
        :param attachments: Array de paths dos anexos (Não Obrigatório)
        '''
        try:
            # setup the parameters of the message
            msg = MIMEMultipart()
            msg['From']=self.sender
            msg['To']=self.__getAllRecipients(recipients)
            msg['Subject']=subject
            msg.attach(MIMEText('<p>' + message + '</p></br>' + self.__getFooter(), 'html'))
            
            # assumes the image is in the current directory
            fp = open(self.logoIMG, 'rb')
            msgImage = MIMEImage(fp.read())
            fp.close()
            
            # define the image's ID as referenced above
            msgImage.add_header('Content-ID', '<logo>')
            msg.attach(msgImage)
            
            if attachments is not None:
                for attachment in attachments:
                    with open(attachment, "rb") as fil:
                        part = MIMEApplication(
                            fil.read(),
                            Name=basename(attachment)
                        )
                    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(attachment)
                    msg.attach(part)
            
            server = smtplib.SMTP(host='smtp-mail.outlook.com', port=587)
            server.starttls()
            server.login(self.sender, self.passwd)
            server.send_message(msg)

        except Exception as e:
            print(e)
        finally:
            if server:
                server.quit()
    
    def send_slack_message(self, channel, message):
        ''' Envia mensagem via Slack \n
        :param channel: Canal para qual será enviada a mensagem ex: #mesa_renda_fixa \n
        :param message: Mensagem que será enviada
        '''
        slack = Slacker(self.slackAPIToken)
        slack.chat.post_message(channel, message, username='EasyBot')